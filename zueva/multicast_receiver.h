//====================================================================
#include <string>
#include <ws2tcpip.h>
//====================================================================
#include "SocketCommon.h"
//====================================================================
class MulticastReceiver
{
public: 
	MulticastReceiver(const std::string& m_group, const std::string& interface, unsigned int port, size_t _maxbufsize)
		:maxbufsize(_maxbufsize)
	{	
		const int yes(1); // for setsockopt

		char port_as_string[6];
		_itoa(port, port_as_string, 10);

		addrinfo hints, *res;
		std::memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_DGRAM;
		getaddrinfo(interface.c_str(), port_as_string, &hints, &res);

		fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		if (fd == INVALID_SOCKET) {
			std::cout<<"Couldn't create socket:"<<std::strerror(errno);
		}
		if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(yes))!=0) {
			std::cout<<"Couldn't set reuse addr option:"<<std::strerror(errno);
		}

		if (bind(fd, res->ai_addr, res->ai_addrlen)!=0) {
			std::cout<<"Couldn't bind:"<<std::strerror(errno);
		}

		ip_mreq mreq;
		mreq.imr_multiaddr.s_addr = inet_addr(m_group.c_str());
		mreq.imr_interface.s_addr = inet_addr(interface.c_str());
		if (setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (const char*)&mreq, sizeof(mreq))!=0) 
		{
			std::cout<<"Couldn't bind to multicast group:"<<std::strerror(errno);
		}
	
	}
	
	//��-��� ������ �� ��������� ����������� �������, �� � �������� ����� ��� �� ����� ��������������� � ������� 
	//�������� msvc, ������� �� ������������ C++11
		
	
	int receive(char* buffer)
	{
		//������� ������ ��� �������� recv
		int iResult = recvfrom(fd, buffer, maxbufsize, 0, 0/*from*/, 0/*fromlen*/);
		if (iResult <= 0)
		{
			perror("Received invalid data");
		}
		return iResult;
		
	}

	~MulticastReceiver()
	{
		closesocket(fd);
	}

private:
	SOCKET fd;
	size_t maxbufsize;

};
