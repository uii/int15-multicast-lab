#include "stdafx.h"

#pragma comment(lib, "ws2_32")

//===========================================================================
#include "multicast_receiver.h"
#include "multicast_sender.h"
#include "protocol.h"

struct Chat
{
	bool* work; 
	std::string multicast_group;
	std::string multicast_interface;
	unsigned int multicast_port;
	std::string nickname;
};

//===========================================================================
void receive_multicast(void* argument);
//===========================================================================

int main()
{
try
{
	WSADATA wsa;
	WSAStartup(0x0202, &wsa);

	//��������� ������, �� � �������� ����� ��������� �� �� ��������� ������ ��� �� ������-������ �����
	const std::string multicast_interface = "127.0.0.1";
	const std::string multicast_group = "230.0.0.1";
	const unsigned int multicast_port = 23000;
	const unsigned int udp_port = 24000; //���� �� ������� ��� ����� �������� �� ��� ������ �� ������������� ����������

	Chat chat;
	chat.multicast_interface = multicast_interface;
	chat.multicast_group = multicast_group;
	chat.multicast_port = multicast_port;

	//�������� ����� ������������ ��������� �������, ���� ������ �������
	unsigned int receiveTimeoutMsec = 1000; //����� �������� ������ �� �������, � ���������� �� ������������� ��������
	bool succeeded = false;
	
	MulticastSender mSender(chat.multicast_group, chat.multicast_port, chat.multicast_interface, 0);

	//������ ����� �����, ������� ����� ��������� ��������� ���������, ��� ����� �����
	const int yes(1); // for setsockopt

	char port_as_string[6];
	_itoa(udp_port, port_as_string, 10);

	addrinfo hints, *res;
	std::memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	getaddrinfo(chat.multicast_interface.c_str(), port_as_string, &hints, &res);

	SOCKET fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (fd == INVALID_SOCKET) {
		std::cout<<"Couldn't create socket:"<<std::strerror(errno);
	}
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(yes))!=0) {
		std::cout<<"Couldn't set reuse addr option:"<<std::strerror(errno);
	}

	timeval to;
	const unsigned int usec = receiveTimeoutMsec*1000;
	to.tv_sec = usec / 1000000;
	to.tv_usec = usec % 1000000;
	if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&to, sizeof(to))<0) {
			std::perror("UDP: couldn't set receive timeout");
		}

	if (bind(fd, res->ai_addr, res->ai_addrlen)!=0) {
		std::perror("UDP: couldn't bind to interface");
	}

	//
	const size_t maxbufsize = 1048;
	char buffer[maxbufsize];
	memset(buffer, 0, maxbufsize);

	while (!succeeded)
	{
		std::cout << "Enter nickname: ";
		std::cin >> chat.nickname;
		NicknameRequest nicknameRequest(chat.nickname, chat.multicast_interface, udp_port);
		mSender.send((char*) &nicknameRequest, sizeof(NicknameRequest));
		int received = recvfrom(fd, buffer, maxbufsize, 0, 0, 0);
		if (received > 0)
		{
			const NicknameReply* nicknameReply = reinterpret_cast<const NicknameReply*> (buffer);
			if (nicknameReply->m_header.messageType == NICKNAME_REPLY)
			{
				continue; // �� ����� ���� ���� ����� ����� ������������ ���� �������
				//�� � ���� ����� ������ ��������� ����� � ������ ��� ����� ��� ����� ��� ������� ���-������ ������
			}
			else
			{
				closesocket(fd);
				succeeded = true;
			}
		}
		else
		{
			
			succeeded = true;
		}
		//succeeded = true; 
	}
	std::cout << "The use of the nickname \"" << chat.nickname << "\" is allowed" << std::endl;
	
	closesocket(fd);	

	bool work = true;
	chat.work = &work;
	std::cout << "Enter message:" << std::endl;
	_beginthread(receive_multicast, 0, &chat);
	std::string message;
	char dump_char = getchar();
	while (true)
	{
		//std::cin >> message;
		std::getline (std::cin, message);
		TextMessage messageToSend (chat.nickname, message);
		mSender.send((char*) &messageToSend, sizeof(TextMessage));
	}
	
	WSACleanup();

	return 0;
}
catch (const std::exception& error) 
{
	int const wsa_error_code = WSAGetLastError();
	std::cerr << "EXCEPTION in main():\n"
			  << "\tMessage: " << error.what() << '\n'
			  << "\tWSAGetLastError(): " << wsa_error_code << " (" << strerror(wsa_error_code) << ")\n"
			  << "\tGetLastError(): " << GetLastError() << '\n'
			  << "Terminating the program." << std::endl;
}
}

void receive_multicast(void* argument)
{	
	const Chat* chat = (const Chat*)argument;

	const size_t bufsize = 2048;
	char buffer [bufsize];
	//������ ������ ��� ���� �����������
	MulticastReceiver mClient(chat->multicast_group, chat->multicast_interface, chat->multicast_port, bufsize);
	while (true)
	{
		memset(buffer, 0, bufsize);
		int received = 	mClient.receive(buffer);
		//�������� ��������� ����������� ������
		const MessageHeader* ticker_header = reinterpret_cast<const MessageHeader*> (buffer);
		//�������� �� ������������ ������� ���������� ������. ���� ���������
		if (ticker_header->payloadlen + sizeof(MessageHeader) >= received)
		{
			std::cout << "received package witn uncorrect len. Received " << received << " bytes. Payload size "  
					<< ticker_header->payloadlen << 
					" header len" << sizeof(MessageHeader) << std::endl;
		}
		MessageType rMessType = ticker_header->messageType;
		if (rMessType == TEXT_MESSAGE)
		{
			const TextMessage* t_message = reinterpret_cast<const TextMessage*> (buffer);
			std::string r_nickname((char*) t_message->nickname);
			if (r_nickname != chat->nickname)
			{
				std::string r_message((char*) t_message->message);
				std::cout << r_nickname << ":" << std::endl;
				std::cout << "\t" << r_message << std::endl;	
			}
		}
		else if (rMessType == NICKNAME_REQUEST)
		{
			const NicknameRequest* nicknane_request_message = reinterpret_cast<const NicknameRequest*> (buffer);
			std::string r_nickname( (char*) nicknane_request_message->nickname);
			if (chat->nickname != r_nickname) 
				continue;
			//����������� ���� ��� ������� ��������� � �������������
			sockaddr_in SendAddr;
			unsigned int targetPort = nicknane_request_message->port;
			std::string targetIP((char*) nicknane_request_message->ip_addr);
			SOCKET sock_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			if (sock_ == INVALID_SOCKET)
				throw std::runtime_error("Couldn't create a UDP socket");

			sockaddr_in loc;
			std::memset(&loc, 0, sizeof(loc));
			loc.sin_family = AF_INET;
			loc.sin_port = htons(0);
			loc.sin_addr.s_addr = inet_addr(chat->multicast_interface.c_str());

			if (bind(sock_, reinterpret_cast<sockaddr*>(&loc), sizeof(loc)) == -1)
				throw std::runtime_error("Couldn't bind to the local host");

			std::memset(&SendAddr, 0, sizeof(SendAddr));
			SendAddr.sin_family = AF_INET;
			SendAddr.sin_port = htons(targetPort);
			SendAddr.sin_addr.s_addr = inet_addr(targetIP.c_str());
			NicknameReply nicknameReply(chat->nickname);

			int iResult = sendto(sock_, (const char*)&nicknameReply, sizeof(NicknameReply), 0,
				(sockaddr *)& SendAddr, sizeof (SendAddr));
			if (iResult == -1)
				std::perror("UDP: couldn't send message");
		
			closesocket(sock_); //��������� ����� �.�. �� ��� ������ �� �����
		}
	
	}	
}
