#ifndef MULTICASTSENDER_H
#define MULTICASTSENDER_H
//====================================================================
#include <string>
#include <cstring>
#include <stdexcept>
//====================================================================
#include "SocketCommon.h"
//====================================================================

class MulticastSender
{
public:
	MulticastSender(
		const std::string& multicastGroup, int multicastPort,
		const std::string& interface, int localPort);
	~MulticastSender();
	void send(const char* buf, size_t len);

private:
	SOCKET sock_;
	sockaddr_in saddr;
	std::string localIP_;
};
//=====================================================================
MulticastSender::MulticastSender(
	const std::string& multicastGroup, int multicastPort,
	const std::string& interface, int localPort)
	
{
	
	struct in_addr iaddr;
	const unsigned char TTL = 10;	

	// set content of struct saddr and imreq to zero
	memset(&saddr, 0, sizeof(struct sockaddr_in));
	memset(&iaddr, 0, sizeof(struct in_addr));

	// open a UDP socket
	sock_ = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock_ == INVALID_SOCKET)
		throw std::runtime_error("Couldn't create socket");

	
	saddr.sin_family = PF_INET;
	saddr.sin_port = htons(localPort);
	saddr.sin_addr.s_addr = htonl(INADDR_ANY); // bind socket to any interface

	if (bind(sock_, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in)) == -1) {
		closesocket(sock_);
		throw std::runtime_error("Couldn't bind local IP");
	}

	iaddr.s_addr = inet_addr(interface.c_str()); // Set the outgoing interface	

	if (setsockopt(sock_, IPPROTO_IP, IP_MULTICAST_IF, (const char*) &iaddr,
		sizeof(struct in_addr)) == -1) {
		closesocket(sock_);
		throw std::runtime_error("Couldn't set the outgoing interface");
	}
	
	if (setsockopt(sock_, IPPROTO_IP, IP_MULTICAST_TTL, (const char*) &TTL,
		sizeof(unsigned char)) == -1) {
		closesocket(sock_);
		throw std::runtime_error("Couldn't change ttl value");
	}

	// set destination multicast address
	saddr.sin_family = PF_INET;
	saddr.sin_addr.s_addr = inet_addr(multicastGroup.c_str());
	saddr.sin_port = htons(multicastPort);
}

void MulticastSender::send(const char* buf, size_t len)
{
	if (sendto(sock_, buf, len, 0, (struct sockaddr *)&saddr, 
		sizeof(struct sockaddr_in)) == -1)
		std::perror("MulticastClient: couldn't send message");
}

MulticastSender::~MulticastSender()
{
	closesocket(sock_);
}


#endif // MULTICASTSENDER_H
