// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <stdio.h>
#include <tchar.h>

#include <cstring>
#include <iostream>
#include <stdexcept>
#include <string>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <process.h>