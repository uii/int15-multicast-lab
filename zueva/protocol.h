#include <string>
#include <cstring>
//===================================================================
#pragma pack (push, 1)
enum MessageType 
{
	TEXT_MESSAGE = 0,
	NICKNAME_REQUEST = 1,
	NICKNAME_REPLY = 2
};

struct MessageHeader
{
	MessageType messageType; //��� ���������
	unsigned int payloadlen; //������ ���� ���������
};

struct TextMessage
{
	TextMessage(const std::string& nname, const std::string& mess)
	{
		m_header.messageType = TEXT_MESSAGE;
		size_t n_len = nname.length();
		size_t mess_len = mess.length();
		memcpy(nickname, nname.c_str(), n_len);
		nickname[n_len] = 0;
		memcpy(message, mess.c_str(), mess_len);
		message[mess_len] = 0;
		m_header.payloadlen = n_len + mess_len;
	}
	MessageHeader m_header;
	char nickname[100];
	char message[1024];
};

struct NicknameRequest
{
	NicknameRequest (const std::string& nname, const std::string& _ip_addr, unsigned short _port)
	{
		m_header.messageType = NICKNAME_REQUEST;
		port = _port;
		size_t n_len = nname.length();
		memcpy(nickname, nname.c_str(), n_len);
		nickname[n_len] = 0;
		size_t ip_adr_len = _ip_addr.length();
		memcpy(ip_addr, _ip_addr.c_str(), ip_adr_len);
		ip_addr[ip_adr_len] = 0;
		m_header.payloadlen = n_len + ip_adr_len + sizeof(port);
	}
	MessageHeader m_header;
	char nickname[100];
	char ip_addr[12];
	unsigned short port;
};

struct NicknameReply
{
	NicknameReply (const std::string& nname)
	{
		m_header.messageType = NICKNAME_REPLY;
		size_t n_len = nname.length();
		memcpy(nickname, nname.c_str(), n_len);
		nickname[n_len] = 0;
		m_header.payloadlen = n_len;
	}
	MessageHeader m_header;
	char nickname[100];
};

#pragma pack (pop)
